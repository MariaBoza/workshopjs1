$(function(){

  var operation = "A"; //"A"=agregar;
	var selected_index = -1; //Índice del elemento de la lista seleccionada

	var tbClients = localStorage.getItem("tbClients");//Recuperar los datos almacenados.

	tbClients = JSON.parse(tbClients); //Convierte cadena a objeto

	if(tbClients == null) //Si no hay datos, inicialice una matriz vacía
		tbClients = [];

//funcion de agregar
	function Add(){
		var client = JSON.stringify({
			Nombre  : $("#txt-name").val(),
			Apellido : $("#txt-apellido").val(),
			Telefono : $("#txt-telefono").val(),
		});
		tbClients.push(client);
		localStorage.setItem("tbClients", JSON.stringify(tbClients));
		return true;
	}


//funcion eliminar
	function Delete(){
		tbClients.splice(selected_index, 1);
		localStorage.setItem("tbClients", JSON.stringify(tbClients));

	}
//se agrega a la tabla
	function List(){
		$("#tblList").html("");
		$("#tblList").html(
			"<thead>"+
			"	<tr>"+
			"	<th></th>"+
			"	<th>Nombre</th>"+
			"	<th>Apellido</th>"+
			"	<th>Telefono</th>"+
			"	</tr>"+
			"</thead>"+
			"<tbody>"+
			"</tbody>"
    );
		for(var i in tbClients){
			var cli = JSON.parse(tbClients[i]);
		  	$("#tblList tbody").append("<tr>"+
									 	 "	<td><img src='/image/delete.png' alt='Delete"+i+"' class='btnDelete'/></td>" +
										 "	<td>"+cli.Nombre+"</td>" +
										 "	<td>"+cli.Apellido+"</td>" +
										 "	<td>"+cli.Telefono+"</td>" +
		  								 "</tr>");
		}
	}
  //agarra todo del form y lo agrega a la listas
  $("#frm-formulario").bind("submit",function(){
		if(operation == "A")
			return Add();
	});
  List();

//elemina a cada una de la listas
	$(".btnDelete").bind("click", function(){
		selected_index = parseInt($(this).attr("alt").replace("Delete", ""));
		Delete();
		List();
	});
});
